## Sugi

Ideas for a new type-oriented programming language.

## Tutorial

### Hello World

In a text editor, type: 

```v
package greeting

main print_hello
  println "hello world"
```

### Comments

```v
// This is a single line comment.
/* 
This is a
multiline comment. 
*/  
```

### Assignment

```v
package example

main modify_variable
  final str name "Bob"
  int age 20
  mod int age 21
```

### Operators
```v
package example

main do_arithmetic
  int sum [+ 3 2]
  mod int sum [+ 4 1]
```
### Functions
```v
package example

void add
  { (func_example)
    final int sum [+ x y]
  }
void sub
  { (func_example)
    final int dif [- x y]
  }
main func_example
  { (add.sum)
    final int x 2
    final int y 5
  } add
  { (sub.dif)
    final int x 4
    final int y 3
  } sub
  println sum dif
```
### If Elif (else if) Else
```v
package example

main if_example
  if [< 10 20]
    println "10 < 20"
  elif [> 10 20]
    println "10 > 20"
  else
    println "10 == 20"
  #if
```
### Arrays
```v
package example

main array_example
  int nums 1 2 3
  final int element [access nums 1]
  replace nums 2 5
  push nums 4 5 6
  println nums
```
### Each Loop
```v
package example

main each_example
  { (loop)
    final str names "Bob" "George" "Henry"
  }
void loop
  each index name names
    println name
  #each
```
